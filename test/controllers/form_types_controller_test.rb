require 'test_helper'

class FormTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @form_type = form_types(:one)
  end

  test "should get index" do
    get form_types_url
    assert_response :success
  end

  test "should create form_type" do
    assert_difference('FormType.count') do
      post form_types_url, params: { form_type: { desc: @form_type.desc, name: @form_type.name, varname: @form_type.varname } }
    end

    assert_response 201
  end

  test "should show form_type" do
    get form_type_url(@form_type)
    assert_response :success
  end

  test "should update form_type" do
    patch form_type_url(@form_type), params: { form_type: { desc: @form_type.desc, name: @form_type.name, varname: @form_type.varname } }
    assert_response 200
  end

  test "should destroy form_type" do
    assert_difference('FormType.count', -1) do
      delete form_type_url(@form_type)
    end

    assert_response 204
  end
end
