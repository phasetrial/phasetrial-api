require 'test_helper'

class FormAppearancesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @form_appearance = form_appearances(:one)
  end

  test "should get index" do
    get form_appearances_url
    assert_response :success
  end

  test "should create form_appearance" do
    assert_difference('FormAppearance.count') do
      post form_appearances_url, params: { form_appearance: {form_type_id: @form_appearance.form_type_id, hidden_vars: @form_appearance.hidden_var_ids, seq: @form_appearance.seq, visit_id: @form_appearance.visit_id } }
    end

    assert_response 201
  end

  test "should show form_appearance" do
    get form_appearance_url(@form_appearance)
    assert_response :success
  end

  test "should update form_appearance" do
    patch form_appearance_url(@form_appearance), params: { form_appearance: {form_type_id: @form_appearance.form_type_id, hidden_vars: @form_appearance.hidden_var_ids, seq: @form_appearance.seq, visit_id: @form_appearance.visit_id } }
    assert_response 200
  end

  test "should destroy form_appearance" do
    assert_difference('FormAppearance.count', -1) do
      delete form_appearance_url(@form_appearance)
    end

    assert_response 204
  end
end
