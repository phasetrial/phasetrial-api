require 'test_helper'

class SitesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @site = sites(:one)
  end

  test "should get index" do
    get sites_url
    assert_response :success
  end

  test "should create site" do
    assert_difference('Site.count') do
      post sites_url, params: { site: { address: @site.address, number: @site.number, timezone: @site.timezone } }
    end

    assert_response 201
  end

  test "should show site" do
    get site_url(@site)
    assert_response :success
  end

  test "should update site" do
    patch site_url(@site), params: { site: { address: @site.address, number: @site.number, timezone: @site.timezone } }
    assert_response 200
  end

  test "should destroy site" do
    assert_difference('Site.count', -1) do
      delete site_url(@site)
    end

    assert_response 204
  end
end
