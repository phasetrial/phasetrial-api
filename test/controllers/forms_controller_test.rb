require 'test_helper'

class FormsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @form = forms(:one)
  end

  test "should get index" do
    get forms_url
    assert_response :success
  end

  test "should create form" do
    assert_difference('Form.count') do
      post forms_url, params: { form: { data: @form.data, form_appearance_id: @form.form_appearance_id, patient_id: @form.patient_id } }
    end

    assert_response 201
  end

  test "should show form" do
    get form_url(@form)
    assert_response :success
  end

  test "should update form" do
    patch form_url(@form), params: { form: { data: @form.data, form_appearance_id: @form.form_appearance_id, patient_id: @form.patient_id } }
    assert_response 200
  end

  test "should destroy form" do
    assert_difference('Form.count', -1) do
      delete form_url(@form)
    end

    assert_response 204
  end
end
