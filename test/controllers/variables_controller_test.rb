require 'test_helper'

class VariablesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @variable = variables(:one)
  end

  test "should get index" do
    get variables_url
    assert_response :success
  end

  test "should create variable" do
    assert_difference('Variable.count') do
      post variables_url, params: { variable: { conditional_id: @variable.conditional_id, form_type_id_id: @variable.form_type_id_id, label: @variable.label, name: @variable.name, type: @variable.type, varname: @variable.varname } }
    end

    assert_response 201
  end

  test "should show variable" do
    get variable_url(@variable)
    assert_response :success
  end

  test "should update variable" do
    patch variable_url(@variable), params: { variable: { conditional_id: @variable.conditional_id, form_type_id_id: @variable.form_type_id_id, label: @variable.label, name: @variable.name, type: @variable.type, varname: @variable.varname } }
    assert_response 200
  end

  test "should destroy variable" do
    assert_difference('Variable.count', -1) do
      delete variable_url(@variable)
    end

    assert_response 204
  end
end
