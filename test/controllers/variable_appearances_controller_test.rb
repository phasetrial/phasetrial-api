require 'test_helper'

class VariableAppearancesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @variable_appearance = variable_appearances(:one)
  end

  test "should get index" do
    get variable_appearances_url
    assert_response :success
  end

  test "should create variable_appearance" do
    assert_difference('VariableAppearance.count') do
      post variable_appearances_url, params: { variable_appearance: { form_type_id: @variable_appearance.form_type_id, seq: @variable_appearance.seq, variable_id: @variable_appearance.variable_id } }
    end

    assert_response 201
  end

  test "should show variable_appearance" do
    get variable_appearance_url(@variable_appearance)
    assert_response :success
  end

  test "should update variable_appearance" do
    patch variable_appearance_url(@variable_appearance), params: { variable_appearance: { form_type_id: @variable_appearance.form_type_id, seq: @variable_appearance.seq, variable_id: @variable_appearance.variable_id } }
    assert_response 200
  end

  test "should destroy variable_appearance" do
    assert_difference('VariableAppearance.count', -1) do
      delete variable_appearance_url(@variable_appearance)
    end

    assert_response 204
  end
end
