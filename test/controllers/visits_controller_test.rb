require 'test_helper'

class VisitsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @visit = visits(:one)
  end

  test "should get index" do
    get visits_url
    assert_response :success
  end

  test "should create visit" do
    assert_difference('Visit.count') do
      post visits_url, params: { visit: { desc: @visit.desc, int: @visit.int, name: @visit.name, seq: @visit.seq, varname: @visit.varname } }
    end

    assert_response 201
  end

  test "should show visit" do
    get visit_url(@visit)
    assert_response :success
  end

  test "should update visit" do
    patch visit_url(@visit), params: { visit: { desc: @visit.desc, int: @visit.int, name: @visit.name, seq: @visit.seq, varname: @visit.varname } }
    assert_response 200
  end

  test "should destroy visit" do
    assert_difference('Visit.count', -1) do
      delete visit_url(@visit)
    end

    assert_response 204
  end
end
