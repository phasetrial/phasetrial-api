class User < ActiveRecord::Base
  belongs_to :role
  before_save do
    self.uid = SecureRandom.uuid
  end

  def as_json(options={})
    super(except: [:created_at, :updated_at],
          methods: [:role_name, :rights, :confirmed?])
  end
  
  def role_name
    self.role.name if self.role
  end
  
  def rights
    self.role.rights if self.role
  end
  
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :confirmable, :omniauthable
  include Overrides::User
end
