class Site < ApplicationRecord
  audited
  has_many :patients
  def as_json(options={})
    super(except: [:created_at, :updated_at])
  end
end
