class Subject < ApplicationRecord
  audited
  belongs_to :site
  def as_json(options={})
    super(except: [:created_at, :updated_at],
          methods: [:site_number])
  end

  def site_number
    self.site.number if self.site
  end
  
end
