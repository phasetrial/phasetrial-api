class FormAppearance < ApplicationRecord
  audited
  belongs_to :visit
  belongs_to :form_type
  def as_json(options={})
    super(except: [:created_at, :updated_at])
  end
  
  def form_name
    self.form_type.name if self.form_type
  end
  
  def log
    self.form_type.log if self.form_type
  end

  def visit_name
    self.visit.name if self.visit
  end
end
