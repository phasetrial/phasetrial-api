class Role < ApplicationRecord
  has_many :users

  def as_json(options={})
    super(except: [:created_at, :updated_at])
  end
  
end
