class FormType < ApplicationRecord
  audited
  has_many :form_appearances
  has_many :variable_appearances
  def as_json(options={})
    super(except: [:created_at, :updated_at],
          include: {
              form_appearances: {
                  except: [:created_at, :updated_at],
                  methods: [:visit_name]
              },
              variable_appearances: {
                  except: [:created_at, :updated_at],
                  methods: [:name, :varname, :label, :datatype, :options]
              }
          }
    )
  end
end
