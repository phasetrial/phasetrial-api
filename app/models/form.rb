class Form < ApplicationRecord
  audited
  belongs_to :subject
  belongs_to :form_appearance
  
  def as_json(options={})
    super(except: [:created_at, :updated_at])
  end
end
