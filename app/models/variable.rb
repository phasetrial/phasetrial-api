class Variable < ApplicationRecord
  audited
  # has_many :conditions, class_name: "Variable", foreign_key: "conditional_id"
  # belongs_to :conditional, class_name: "Variable"
  has_many :variable_appearances
  
  def as_json(options={})
    super(except: [:created_at, :updated_at],
          include: {
              variable_appearances: {
                  except: [:created_at, :updated_at],
                  methods: [:form_name]
              }
          }
    )
  end
  
  enum datatype: {
      integer: 'integer',
      text: 'text',
      float: 'float',
      datetime: 'datetime',
      radio: 'radio',
      checkbox: 'checkbox'
  }
end
