class VariableAppearance < ApplicationRecord
  audited
  belongs_to :form_type
  belongs_to :variable
  def as_json(options={})
    super(except: [:created_at, :updated_at])
  end

  def form_name
    self.form_type.name if self.form_type
  end

  def name
    self.variable.name if self.variable
  end

  def varname
    self.variable.varname if self.variable
  end

  def label
    self.variable.label if self.variable
  end

  def datatype
    self.variable.datatype if self.variable
  end

  def options
    self.variable.options if self.variable
  end
end
