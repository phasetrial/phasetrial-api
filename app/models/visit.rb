class Visit < ApplicationRecord
  audited
  has_many :form_appearances
  def as_json(options={})
    super(except: [:created_at, :updated_at],
          include: {
              form_appearances: {
                  except: [:created_at, :updated_at],
                  methods: [:form_name, :log]
              }
          }
    )
  end
end
