class VariableAppearancesController < ApplicationController
  before_action :set_variable_appearance, only: [:show, :update, :destroy]

  # GET /variable_appearances
  def index
    @variable_appearances = VariableAppearance.all

    render json: @variable_appearances
  end

  # GET /variable_appearances/find
  def find
    @variable_appearance = VariableAppearance.where(find_variable_appearance)
    render json: @variable_appearance
  end

  # GET /variable_appearances/1
  def show
    render json: @variable_appearance
  end

  # POST /variable_appearances
  def create
    @variable_appearance = VariableAppearance.new(variable_appearance_params)

    if @variable_appearance.save
      render json: @variable_appearance, status: :created, location: @variable_appearance
    else
      render json: @variable_appearance.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /variable_appearances/1
  def update
    if @variable_appearance.update(variable_appearance_params)
      render json: @variable_appearance
    else
      render json: @variable_appearance.errors, status: :unprocessable_entity
    end
  end

  # DELETE /variable_appearances/1
  def destroy
    @variable_appearance.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_variable_appearance
      @variable_appearance = VariableAppearance.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def variable_appearance_params
      params.require(:variable_appearance).permit(:form_type_id, :variable_id, :seq)
    end

    def find_variable_appearance
      params.permit(:variable_id, :form_type_id)
    end
end
