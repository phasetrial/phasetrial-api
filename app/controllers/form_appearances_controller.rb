class FormAppearancesController < ApplicationController
  before_action :set_form_appearance, only: [:show, :update, :destroy]

  # GET /form_appearances
  def index
    @form_appearances = FormAppearance.all

    render json: @form_appearances
  end
  
  # GET /form_appearances/find
  def find
    @form_appearance = FormAppearance.where(find_form_appearance)
    render json: @form_appearance
  end

  # GET /form_appearances/1
  def show
    render json: @form_appearance
  end

  # POST /form_appearances
  def create
    @form_appearance = FormAppearance.new(form_appearance_params)

    if @form_appearance.save
      render json: @form_appearance, status: :created, location: @form_appearance
    else
      render json: @form_appearance.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /form_appearances/1
  def update
    if @form_appearance.update(form_appearance_params)
      render json: @form_appearance
    else
      render json: @form_appearance.errors, status: :unprocessable_entity
    end
  end

  # DELETE /form_appearances/1
  def destroy
    @form_appearance.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_form_appearance
      @form_appearance = FormAppearance.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def form_appearance_params
      params.require(:form_appearance).permit(:visit_id, :form_type_id, :seq, :hidden_vars)
    end
  
    def find_form_appearance
      params.permit(:visit_id, :form_type_id)
    end
end
