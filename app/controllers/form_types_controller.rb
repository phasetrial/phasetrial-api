class FormTypesController < ApplicationController
  before_action :set_form_type, only: [:show, :update, :destroy]

  # GET /form_types
  def index
    @form_types = FormType.all

    render json: @form_types
  end

  # GET /form_types/1
  def show
    render json: @form_type
  end

  # POST /form_types
  def create
    @form_type = FormType.new(form_type_params)

    if @form_type.save
      render json: @form_type, status: :created, location: @form_type
    else
      render json: @form_type.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /form_types/1
  def update
    if @form_type.update(form_type_params)
      render json: @form_type
    else
      render json: @form_type.errors, status: :unprocessable_entity
    end
  end

  # DELETE /form_types/1
  def destroy
    @form_type.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_form_type
      @form_type = FormType.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def form_type_params
      params.require(:form_type).permit(:name, :varname, :desc, :log)
    end
end
