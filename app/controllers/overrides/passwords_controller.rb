module Overrides
  class PasswordsController < DeviseTokenAuth::PasswordsController
    def edit
      @resource = resource_class.reset_password_by_token({
                                                             reset_password_token: resource_params[:reset_password_token]
                                                         })

      if @resource and @resource.id
        client_id  = SecureRandom.urlsafe_base64(nil, false)
        token      = SecureRandom.urlsafe_base64(nil, false)
        token_hash = BCrypt::Password.create(token)
        expiry     = (Time.now + DeviseTokenAuth.token_lifespan).to_i

        @resource.tokens[client_id] = {
            token:  token_hash,
            expiry: expiry
        }

        # ensure that user is confirmed
        @resource.skip_confirmation! if @resource.devise_modules.include?(:confirmable) && !@resource.confirmed_at

        # allow user to change password once without current_password
        @resource.allow_password_change = true

        @resource.save!
        
        yield if block_given?

        redirect_to(Overrides::Url.generate(params[:redirect_url], {
            token:          token,
            client_id:      client_id,
            reset_password: true,
            uid:            @resource.uid,
            config:         params[:config]
        }))
      else
        render_edit_error
      end
    end
    def update
      # make sure user is authorized
      unless @resource
        return render_update_error_unauthorized
      end

      # make sure account doesn't use oauth2 provider
      unless @resource.provider == 'email'
        return render_update_error_password_not_required
      end

      # ensure that password params were sent
      unless password_resource_params[:password] and password_resource_params[:password_confirmation]
        return render_update_error_missing_password
      end

      if @resource.send(resource_update_method, password_resource_params)
        @resource.allow_password_change = false
        @resource.save!

        yield if block_given?
        return render_update_success
      else
        return render_update_error
      end
    end
  end
end