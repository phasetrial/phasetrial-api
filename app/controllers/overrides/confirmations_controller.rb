module Overrides
  class ConfirmationsController < DeviseTokenAuth::ApplicationController
    def show
      @resource = resource_class.confirm_by_token(params[:confirmation_token])

      if @resource and @resource.id

        @resource.save!

        yield if block_given?

        redirect_to(Overrides::Url.generate(params[:redirect_url], {
            uid: @resource.uid,
            account_confirmation_success: true,
            locked: @resource.locked
        }))
      else
        raise ActionController::RoutingError.new('Not Found')
      end
    end
  end
end