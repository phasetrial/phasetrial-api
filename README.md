# Getting started

Here are some learning resources to get you started with Ruby, Rails, and AWS:  

### Codeacademy
[Ruby](https://www.codecademy.com/learn/ruby)  
[Rails](https://www.codecademy.com/learn/learn-rails) 
[SQL](https://www.codecademy.com/learn/learn-sql)

### Udemy
[AWS Certified Solutions Architect - Associate 2016](https://www.udemy.com/aws-certified-solutions-architect-associate/learn/v4/overview)  
[AWS Certified Developer - Associate 2016](https://www.udemy.com/aws-certified-developer-associate/learn/v4/overview)  

-------------

# Gems

Here are the important gems I use in the Rails app. Feel free to check out the Gemfile too.

###### [Devise Token Auth](https://github.com/lynndylanhurley/devise_token_auth)

Devise Token Auth handles the user authentication at the frontend. It works with the [ng-token-auth](https://github.com/lynndylanhurley/ng-token-auth) package on the frontend to set up new users, do password resets, make sure users are logged in before accessing the app, etc. When you log in, it stores a token in your browser cookies so that it knows you're still logged in when you come back later on. You can also set expiration on the tokens so that there's an inactivity period.

###### [Audited](https://github.com/collectiveidea/audited)

Audited saves all changes to data in an audit trail table in the database. I haven't incorporated this into the UI yet, but when you save data, you'll see in the log that an entry is added to the 'audited' table.

###### [Rack CORS](https://github.com/cyu/rack-cors)

This gem allows the frontend code to talk to the Rails app. Normally, your browser blocks requests across hosts. Since the browser code is at xxx.phasetrial.com, and the Rails app is at xxx.rails.phasetrial.com, requests across these hosts are blocked. This gem adds a few headers to allow these two hosts to talk to each other.

###### [AWS SDK](https://github.com/aws/aws-sdk-ruby) & [Fog AWS](https://github.com/fog/fog-aws)

The AWS SDK gem allows us to do pretty much anything in AWS in Ruby code. The Fog gem makes it a lot simpler to set up a mailer using AWS Simple Email Service.

# Database / Model Dependency Structure

Take a look at the diagram below:

![Diagram](https://i.imgsafe.org/c2591e4.png "Model Diagram")

# AWS Environment Setup

This will only work on my local machine right now since I have access to the phasetrial.com domain. However, down the line I can create an IAM user for each of you so that you'll be able to do this as well.

First off, you'll have to set up your own AWS account and get the [CLI](https://aws.amazon.com/cli/) locally. You'll also need the [Elastic Beanstalk CLI](http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3.html). Once you have your credentials entered, you can open a terminal in this project and run `./phasetrial setup trialname`. If you want to just do `phasetrial setup trialname`, you'll need the project path to be in you PATH environment variable. That will setup a new trial environment that will be available at https://trialname.phasetrial.com with a fresh new database. You can login with `clydedroid@gmail.com` and `PhaseSuperuser`.

We can do all this just from the CloudFormation template in `cloudformation.json`. Take a look at it to see all the resources it sets up for each environment.