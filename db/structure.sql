--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: datatype; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE datatype AS ENUM (
    'integer',
    'text',
    'float',
    'datetime',
    'radio',
    'checkbox'
);


--
-- Name: status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE status AS ENUM (
    'Screening',
    'Screen Failure',
    'Enrolled',
    'Randomized',
    'Discontinued Treatment',
    'Open Label'
);


--
-- Name: timezones; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE timezones AS ENUM (
    '(UTC-12:00) International Date Line West',
    '(UTC-11:00) Coordinated Universal Time-11',
    '(UTC-10:00) Hawaii',
    '(UTC-09:00) Alaska',
    '(UTC-08:00) Baja California',
    '(UTC-08:00) Pacific Time (US & Canada)',
    '(UTC-07:00) Arizona',
    '(UTC-07:00) Chihuahua, La Paz, Mazatlan',
    '(UTC-07:00) Mountain Time (US & Canada)',
    '(UTC-06:00) Central America',
    '(UTC-06:00) Central Time (US & Canada)',
    '(UTC-06:00) Guadalajara, Mexico City, Monterrey',
    '(UTC-06:00) Saskatchewan',
    '(UTC-05:00) Bogota, Lima, Quito',
    '(UTC-05:00) Eastern Time (US & Canada)',
    '(UTC-05:00) Indiana (East)',
    '(UTC-04:30) Caracas',
    '(UTC-04:00) Asuncion',
    '(UTC-04:00) Atlantic Time (Canada)',
    '(UTC-04:00) Cuiaba',
    '(UTC-04:00) Georgetown, La Paz, Manaus, San Juan',
    '(UTC-04:00) Santiago',
    '(UTC-03:30) Newfoundland',
    '(UTC-03:00) Brasilia',
    '(UTC-03:00) Buenos Aires',
    '(UTC-03:00) Cayenne, Fortaleza',
    '(UTC-03:00) Greenland',
    '(UTC-03:00) Montevideo',
    '(UTC-03:00) Salvador',
    '(UTC-02:00) Coordinated Universal Time-02',
    '(UTC-02:00) Mid-Atlantic - Old',
    '(UTC-01:00) Azores',
    '(UTC-01:00) Cape Verde Is.',
    '(UTC) Casablanca',
    '(UTC) Coordinated Universal Time',
    '(UTC) Dublin, Edinburgh, Lisbon, London',
    '(UTC) Monrovia, Reykjavik',
    '(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna',
    '(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague',
    '(UTC+01:00) Brussels, Copenhagen, Madrid, Paris',
    '(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb',
    '(UTC+01:00) West Central Africa',
    '(UTC+01:00) Windhoek',
    '(UTC+02:00) Athens, Bucharest',
    '(UTC+02:00) Beirut',
    '(UTC+02:00) Cairo',
    '(UTC+02:00) Damascus',
    '(UTC+02:00) E. Europe',
    '(UTC+02:00) Harare, Pretoria',
    '(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius',
    '(UTC+02:00) Istanbul',
    '(UTC+02:00) Jerusalem',
    '(UTC+02:00) Tripoli',
    '(UTC+03:00) Amman',
    '(UTC+03:00) Baghdad',
    '(UTC+03:00) Kaliningrad, Minsk',
    '(UTC+03:00) Kuwait, Riyadh',
    '(UTC+03:00) Nairobi',
    '(UTC+03:30) Tehran',
    '(UTC+04:00) Abu Dhabi, Muscat',
    '(UTC+04:00) Baku',
    '(UTC+04:00) Moscow, St. Petersburg, Volgograd',
    '(UTC+04:00) Port Louis',
    '(UTC+04:00) Tbilisi',
    '(UTC+04:00) Yerevan',
    '(UTC+04:30) Kabul',
    '(UTC+05:00) Ashgabat, Tashkent',
    '(UTC+05:00) Islamabad, Karachi',
    '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi',
    '(UTC+05:30) Sri Jayawardenepura',
    '(UTC+05:45) Kathmandu',
    '(UTC+06:00) Astana',
    '(UTC+06:00) Dhaka',
    '(UTC+06:00) Ekaterinburg',
    '(UTC+06:30) Yangon (Rangoon)',
    '(UTC+07:00) Bangkok, Hanoi, Jakarta',
    '(UTC+07:00) Novosibirsk',
    '(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi',
    '(UTC+08:00) Krasnoyarsk',
    '(UTC+08:00) Kuala Lumpur, Singapore',
    '(UTC+08:00) Perth',
    '(UTC+08:00) Taipei',
    '(UTC+08:00) Ulaanbaatar',
    '(UTC+09:00) Irkutsk',
    '(UTC+09:00) Osaka, Sapporo, Tokyo',
    '(UTC+09:00) Seoul',
    '(UTC+09:30) Adelaide',
    '(UTC+09:30) Darwin',
    '(UTC+10:00) Brisbane',
    '(UTC+10:00) Canberra, Melbourne, Sydney',
    '(UTC+10:00) Guam, Port Moresby',
    '(UTC+10:00) Hobart',
    '(UTC+10:00) Yakutsk',
    '(UTC+11:00) Solomon Is., New Caledonia',
    '(UTC+11:00) Vladivostok',
    '(UTC+12:00) Auckland, Wellington',
    '(UTC+12:00) Coordinated Universal Time+12',
    '(UTC+12:00) Fiji',
    '(UTC+12:00) Magadan',
    '(UTC+12:00) Petropavlovsk-Kamchatsky - Old',
    '(UTC+13:00) Nukualofa',
    '(UTC+13:00) Samoa'
);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: audits; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE audits (
    id integer NOT NULL,
    auditable_id integer,
    auditable_type character varying,
    associated_id integer,
    associated_type character varying,
    user_id integer,
    user_type character varying,
    username character varying,
    action character varying,
    audited_changes text,
    version integer DEFAULT 0,
    comment character varying,
    remote_address character varying,
    request_uuid character varying,
    created_at timestamp without time zone
);


--
-- Name: audits_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE audits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: audits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE audits_id_seq OWNED BY audits.id;


--
-- Name: configs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE configs (
    id integer NOT NULL,
    name character varying,
    sponsor character varying,
    phase character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: configs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE configs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: configs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE configs_id_seq OWNED BY configs.id;


--
-- Name: form_appearances; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE form_appearances (
    id integer NOT NULL,
    visit_id integer,
    form_type_id integer,
    seq integer,
    hidden_vars integer[] DEFAULT '{}'::integer[],
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: form_appearances_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE form_appearances_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: form_appearances_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE form_appearances_id_seq OWNED BY form_appearances.id;


--
-- Name: form_types; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE form_types (
    id integer NOT NULL,
    name character varying,
    varname character varying,
    "desc" text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    log boolean
);


--
-- Name: form_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE form_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: form_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE form_types_id_seq OWNED BY form_types.id;


--
-- Name: forms; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE forms (
    id integer NOT NULL,
    subject_id integer,
    form_appearance_id integer,
    data jsonb DEFAULT '{}'::jsonb,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: forms_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE forms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: forms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE forms_id_seq OWNED BY forms.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE roles (
    id integer NOT NULL,
    name character varying,
    rights character varying[] DEFAULT '{}'::character varying[],
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: sites; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE sites (
    id integer NOT NULL,
    number character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    address jsonb,
    timezone timezones
);


--
-- Name: sites_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE sites_id_seq OWNED BY sites.id;


--
-- Name: subjects; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE subjects (
    id integer NOT NULL,
    number character varying,
    site_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    status status
);


--
-- Name: subjects_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE subjects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: subjects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE subjects_id_seq OWNED BY subjects.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    provider character varying DEFAULT 'email'::character varying NOT NULL,
    uid character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying,
    last_sign_in_ip character varying,
    confirmation_token character varying,
    confirmed_at timestamp without time zone,
    confirmation_sent_at timestamp without time zone,
    unconfirmed_email character varying,
    name character varying,
    nickname character varying,
    image character varying,
    email character varying,
    tokens json,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    locked boolean DEFAULT true,
    role_id integer,
    allow_password_change boolean DEFAULT false
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: variable_appearances; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE variable_appearances (
    id integer NOT NULL,
    form_type_id integer,
    variable_id integer,
    seq integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: variable_appearances_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE variable_appearances_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: variable_appearances_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE variable_appearances_id_seq OWNED BY variable_appearances.id;


--
-- Name: variables; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE variables (
    id integer NOT NULL,
    name character varying,
    varname character varying,
    label character varying,
    conditional_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    datatype datatype,
    options character varying[] DEFAULT '{}'::character varying[],
    length integer,
    "precision" integer,
    required boolean,
    unknown character varying[] DEFAULT '{}'::character varying[]
);


--
-- Name: variables_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE variables_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: variables_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE variables_id_seq OWNED BY variables.id;


--
-- Name: visits; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE visits (
    id integer NOT NULL,
    name character varying,
    varname character varying,
    "desc" text,
    seq integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: visits_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE visits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: visits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE visits_id_seq OWNED BY visits.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY audits ALTER COLUMN id SET DEFAULT nextval('audits_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY configs ALTER COLUMN id SET DEFAULT nextval('configs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY form_appearances ALTER COLUMN id SET DEFAULT nextval('form_appearances_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY form_types ALTER COLUMN id SET DEFAULT nextval('form_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY forms ALTER COLUMN id SET DEFAULT nextval('forms_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY sites ALTER COLUMN id SET DEFAULT nextval('sites_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY subjects ALTER COLUMN id SET DEFAULT nextval('subjects_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY variable_appearances ALTER COLUMN id SET DEFAULT nextval('variable_appearances_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY variables ALTER COLUMN id SET DEFAULT nextval('variables_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY visits ALTER COLUMN id SET DEFAULT nextval('visits_id_seq'::regclass);


--
-- Name: ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: audits_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY audits
    ADD CONSTRAINT audits_pkey PRIMARY KEY (id);


--
-- Name: configs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY configs
    ADD CONSTRAINT configs_pkey PRIMARY KEY (id);


--
-- Name: form_appearances_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY form_appearances
    ADD CONSTRAINT form_appearances_pkey PRIMARY KEY (id);


--
-- Name: form_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY form_types
    ADD CONSTRAINT form_types_pkey PRIMARY KEY (id);


--
-- Name: forms_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY forms
    ADD CONSTRAINT forms_pkey PRIMARY KEY (id);


--
-- Name: roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: sites_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY sites
    ADD CONSTRAINT sites_pkey PRIMARY KEY (id);


--
-- Name: subjects_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY subjects
    ADD CONSTRAINT subjects_pkey PRIMARY KEY (id);


--
-- Name: uniqueformbysub; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY forms
    ADD CONSTRAINT uniqueformbysub UNIQUE (subject_id, form_appearance_id);


--
-- Name: uniquesitenum; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY sites
    ADD CONSTRAINT uniquesitenum UNIQUE (number);


--
-- Name: uniquesubnum; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY subjects
    ADD CONSTRAINT uniquesubnum UNIQUE (number, site_id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: variable_appearances_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY variable_appearances
    ADD CONSTRAINT variable_appearances_pkey PRIMARY KEY (id);


--
-- Name: variables_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY variables
    ADD CONSTRAINT variables_pkey PRIMARY KEY (id);


--
-- Name: visits_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY visits
    ADD CONSTRAINT visits_pkey PRIMARY KEY (id);


--
-- Name: associated_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX associated_index ON audits USING btree (associated_id, associated_type);


--
-- Name: auditable_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auditable_index ON audits USING btree (auditable_id, auditable_type);


--
-- Name: index_audits_on_created_at; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_audits_on_created_at ON audits USING btree (created_at);


--
-- Name: index_audits_on_request_uuid; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_audits_on_request_uuid ON audits USING btree (request_uuid);


--
-- Name: index_form_appearances_on_form_type_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_form_appearances_on_form_type_id ON form_appearances USING btree (form_type_id);


--
-- Name: index_form_appearances_on_visit_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_form_appearances_on_visit_id ON form_appearances USING btree (visit_id);


--
-- Name: index_forms_on_form_appearance_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_forms_on_form_appearance_id ON forms USING btree (form_appearance_id);


--
-- Name: index_forms_on_subject_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_forms_on_subject_id ON forms USING btree (subject_id);


--
-- Name: index_subjects_on_site_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_subjects_on_site_id ON subjects USING btree (site_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON users USING btree (reset_password_token);


--
-- Name: index_users_on_role_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_users_on_role_id ON users USING btree (role_id);


--
-- Name: index_users_on_uid_and_provider; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_uid_and_provider ON users USING btree (uid, provider);


--
-- Name: index_variable_appearances_on_form_type_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_variable_appearances_on_form_type_id ON variable_appearances USING btree (form_type_id);


--
-- Name: index_variable_appearances_on_variable_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_variable_appearances_on_variable_id ON variable_appearances USING btree (variable_id);


--
-- Name: index_variables_on_conditional_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_variables_on_conditional_id ON variables USING btree (conditional_id);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: user_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX user_index ON audits USING btree (user_id, user_type);


--
-- Name: fk_rails_50683d7ed7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY forms
    ADD CONSTRAINT fk_rails_50683d7ed7 FOREIGN KEY (form_appearance_id) REFERENCES form_appearances(id);


--
-- Name: fk_rails_529005da40; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY forms
    ADD CONSTRAINT fk_rails_529005da40 FOREIGN KEY (subject_id) REFERENCES subjects(id);


--
-- Name: fk_rails_642f17018b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT fk_rails_642f17018b FOREIGN KEY (role_id) REFERENCES roles(id);


--
-- Name: fk_rails_86c3bddc4b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY form_appearances
    ADD CONSTRAINT fk_rails_86c3bddc4b FOREIGN KEY (visit_id) REFERENCES visits(id);


--
-- Name: fk_rails_94c6d0945c; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY variable_appearances
    ADD CONSTRAINT fk_rails_94c6d0945c FOREIGN KEY (form_type_id) REFERENCES form_types(id);


--
-- Name: fk_rails_b89f969fcc; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY variable_appearances
    ADD CONSTRAINT fk_rails_b89f969fcc FOREIGN KEY (variable_id) REFERENCES variables(id);


--
-- Name: fk_rails_dbea9f883f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY subjects
    ADD CONSTRAINT fk_rails_dbea9f883f FOREIGN KEY (site_id) REFERENCES sites(id);


--
-- Name: fk_rails_e867a2f396; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY form_appearances
    ADD CONSTRAINT fk_rails_e867a2f396 FOREIGN KEY (form_type_id) REFERENCES form_types(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user",public;

INSERT INTO schema_migrations (version) VALUES ('20160118003920'), ('20160118004451'), ('20160118005431'), ('20160118010744'), ('20160118011321'), ('20160118011430'), ('20160118012136'), ('20160118012415'), ('20160131165838'), ('20160223214044'), ('20160306162859'), ('20160306182129'), ('20160309214405'), ('20160310171547'), ('20160314192233'), ('20160314192420'), ('20160316134329'), ('20160327182727'), ('20160407194224'), ('20160407194421'), ('20160407195333'), ('20160418153926'), ('20160422143531'), ('20160426000728'), ('20160426001339'), ('20160427203055'), ('20160428162218'), ('20160429181540');


