# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160429181540) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "audits", force: :cascade do |t|
    t.integer  "auditable_id"
    t.string   "auditable_type"
    t.integer  "associated_id"
    t.string   "associated_type"
    t.integer  "user_id"
    t.string   "user_type"
    t.string   "username"
    t.string   "action"
    t.text     "audited_changes"
    t.integer  "version",         default: 0
    t.string   "comment"
    t.string   "remote_address"
    t.string   "request_uuid"
    t.datetime "created_at"
    t.index ["associated_id", "associated_type"], name: "associated_index", using: :btree
    t.index ["auditable_id", "auditable_type"], name: "auditable_index", using: :btree
    t.index ["created_at"], name: "index_audits_on_created_at", using: :btree
    t.index ["request_uuid"], name: "index_audits_on_request_uuid", using: :btree
    t.index ["user_id", "user_type"], name: "user_index", using: :btree
  end

  create_table "configs", force: :cascade do |t|
    t.string   "name"
    t.string   "sponsor"
    t.string   "phase"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "form_appearances", force: :cascade do |t|
    t.integer  "visit_id"
    t.integer  "form_type_id"
    t.integer  "seq"
    t.integer  "hidden_vars",  default: [],              array: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["form_type_id"], name: "index_form_appearances_on_form_type_id", using: :btree
    t.index ["visit_id"], name: "index_form_appearances_on_visit_id", using: :btree
  end

  create_table "form_types", force: :cascade do |t|
    t.string   "name"
    t.string   "varname"
    t.text     "desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean  "log"
  end

  create_table "forms", force: :cascade do |t|
    t.integer  "subject_id"
    t.integer  "form_appearance_id"
    t.jsonb    "data",               default: {}
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.index ["form_appearance_id"], name: "index_forms_on_form_appearance_id", using: :btree
    t.index ["subject_id", "form_appearance_id"], name: "uniqueformbysub", unique: true, using: :btree
    t.index ["subject_id"], name: "index_forms_on_subject_id", using: :btree
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.string   "rights",     default: [],              array: true
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

# Could not dump table "sites" because of following StandardError
#   Unknown type 'timezones' for column 'timezone'

# Could not dump table "subjects" because of following StandardError
#   Unknown type 'status' for column 'status'

  create_table "users", force: :cascade do |t|
    t.string   "provider",               default: "email", null: false
    t.string   "uid",                    default: "",      null: false
    t.string   "encrypted_password",     default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "name"
    t.string   "nickname"
    t.string   "image"
    t.string   "email"
    t.json     "tokens"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "locked",                 default: true
    t.integer  "role_id"
    t.boolean  "allow_password_change",  default: false
    t.index ["email"], name: "index_users_on_email", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["role_id"], name: "index_users_on_role_id", using: :btree
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true, using: :btree
  end

  create_table "variable_appearances", force: :cascade do |t|
    t.integer  "form_type_id"
    t.integer  "variable_id"
    t.integer  "seq"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["form_type_id"], name: "index_variable_appearances_on_form_type_id", using: :btree
    t.index ["variable_id"], name: "index_variable_appearances_on_variable_id", using: :btree
  end

# Could not dump table "variables" because of following StandardError
#   Unknown type 'datatype' for column 'datatype'

  create_table "visits", force: :cascade do |t|
    t.string   "name"
    t.string   "varname"
    t.text     "desc"
    t.integer  "seq"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "form_appearances", "form_types"
  add_foreign_key "form_appearances", "visits"
  add_foreign_key "forms", "form_appearances"
  add_foreign_key "forms", "subjects"
  add_foreign_key "subjects", "sites"
  add_foreign_key "users", "roles"
  add_foreign_key "variable_appearances", "form_types"
  add_foreign_key "variable_appearances", "variables"
end
