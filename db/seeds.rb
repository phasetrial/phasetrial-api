# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Role.create(name: "Superuser", rights: ["userAdmin", "roleAdmin", "siteAdmin", "dev", "test", "dataEntry", "dataReview", "dataManagement", "query", "sign"]) unless Role.all.first
User.create(provider: "email", uid: "clydedroid@gmail.com", password: "PhaseSuperuser", name: "Nick Clyde", confirmation_token: "Jm1xPbp7UtPtqybcPjhm", confirmed_at: "2016-05-05 00:09:10", confirmation_sent_at: "2016-05-05 00:06:12", email: "clydedroid@gmail.com", locked: false, role_id: 1, allow_password_change: false) unless User.all.first
