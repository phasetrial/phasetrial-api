class CreateVariableAppearances < ActiveRecord::Migration[5.0]
  def change
    create_table :variable_appearances do |t|
      t.references :form_type, foreign_key: true
      t.references :variable, foreign_key: true
      t.integer :seq

      t.timestamps
    end
  end
end
