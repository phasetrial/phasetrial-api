class CreateVisits < ActiveRecord::Migration[5.0]
  def change
    create_table :visits do |t|
      t.string :name
      t.string :varname
      t.text :desc
      t.integer :seq

      t.timestamps
    end
  end
end
