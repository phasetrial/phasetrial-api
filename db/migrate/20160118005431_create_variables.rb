class CreateVariables < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
      CREATE TYPE type AS ENUM ('integer', 'text', 'float', 'datetime', 'radio', 'checkbox');
    SQL

    create_table :variables do |t|
      t.string :name
      t.string :varname
      t.string :label
      t.references :conditional, index: true
      t.references :form_type, index: true, foreign_key: true

      t.timestamps
    end

    add_column :variables, :type, :type, index: true
  end

  def down
    drop_table :variables

    execute <<-SQL
      DROP TYPE type;
    SQL
  end
end
