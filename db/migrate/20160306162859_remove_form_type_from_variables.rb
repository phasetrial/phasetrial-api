class RemoveFormTypeFromVariables < ActiveRecord::Migration[5.0]
  def up
    remove_column :variables, :form_type_id
  end
  def down
    add_reference :variables, :form_type, index: true
  end
end
