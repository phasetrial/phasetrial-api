class RenamePatientsToSubjects < ActiveRecord::Migration[5.0]
  def change
    rename_table :patients, :subjects
  end
end
