class AddOptionsToVariables < ActiveRecord::Migration[5.0]
  def change
    add_column :variables, :options, :string, array: true, default: []
  end
end
