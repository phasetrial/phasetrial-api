class CreateForms < ActiveRecord::Migration[5.0]
  def change
    create_table :forms do |t|
      t.references :patient, index: true, foreign_key: true
      t.references :form_appearance, index: true, foreign_key: true
      t.jsonb :data, default: '{}'

      t.timestamps
    end
  end
end
