class CreateRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :roles do |t|
      t.string :name
      t.string :rights, array: true, default: []

      t.timestamps
    end
  end
end
