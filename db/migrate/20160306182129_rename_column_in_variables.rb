class RenameColumnInVariables < ActiveRecord::Migration[5.0]
  def change
    rename_column :variables, :type, :datatype
  end
end
