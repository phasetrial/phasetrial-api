class AddColumnsToVariables < ActiveRecord::Migration[5.0]
  def change
    add_column :variables, :length, :integer
    add_column :variables, :precision, :integer
    add_column :variables, :required, :boolean
    add_column :variables, :unknown, :string, array: true, default: []
  end
end
