class RemoveColumnsFromSites < ActiveRecord::Migration[5.0]
  def change
    remove_column :sites, :address, :string
    remove_column :sites, :timezone, :string
  end
end
