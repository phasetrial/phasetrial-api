class CreateSites < ActiveRecord::Migration[5.0]
  def change
    create_table :sites do |t|
      t.string :number
      t.string :address
      t.string :timezone

      t.timestamps
    end
  end
end
