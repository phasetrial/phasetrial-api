class CreateFormAppearances < ActiveRecord::Migration[5.0]
  def change
    create_table :form_appearances do |t|
      t.references :visit, index: true, foreign_key: true
      t.references :form_type, index: true, foreign_key: true
      t.integer :seq
      t.integer :hidden_var_ids, array: true, default: []

      t.timestamps
    end
  end
end
