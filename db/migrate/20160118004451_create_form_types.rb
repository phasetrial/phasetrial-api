class CreateFormTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :form_types do |t|
      t.string :name
      t.string :varname
      t.text :desc

      t.timestamps
    end
  end
end
