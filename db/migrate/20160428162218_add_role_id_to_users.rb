class AddRoleIdToUsers < ActiveRecord::Migration[5.0]
  def change
    add_reference :users, :role, index: true, foreign_key: true
    drop_table :user_roles do end
  end
end
