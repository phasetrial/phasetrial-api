class DropForms < ActiveRecord::Migration[5.0]
  def change
    drop_table :forms
  end
end
