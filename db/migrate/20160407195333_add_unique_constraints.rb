class AddUniqueConstraints < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
      ALTER TABLE sites ADD CONSTRAINT UniqueSiteNum UNIQUE (number);
      ALTER TABLE subjects ADD CONSTRAINT UniqueSubNum UNIQUE (number, site_id);
      ALTER TABLE forms ADD CONSTRAINT UniqueFormBySub UNIQUE (subject_id, form_appearance_id);
    SQL
  end
  def down
    execute <<-SQL
      ALTER TABLE sites DROP CONSTRAINT UniqueSiteNum;
      ALTER TABLE subjects DROP CONSTRAINT UniqueSubNum;
      ALTER TABLE forms DROP CONSTRAINT UniqueFormBySub;
    SQL
  end
end