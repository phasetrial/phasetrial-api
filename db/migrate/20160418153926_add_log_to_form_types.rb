class AddLogToFormTypes < ActiveRecord::Migration[5.0]
  def change
    add_column :form_types, :log, :boolean
  end
end
