class CreatePatients < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
      CREATE TYPE status AS ENUM ('Screening', 'Screen Failure', 'Enrolled', 'Randomized', 'Discontinued Treatment', 'Open Label');
    SQL

    create_table :patients do |t|
      t.string :number
      t.references :site, index: true, foreign_key: true

      t.timestamps
    end

    add_column :patients, :status, :status, index: true
  end

  def down
    drop_table :patients

    execute <<-SQL
      DROP TYPE status;
    SQL
  end
end
