Devise.setup do |config|
  # The e-mail address that mail will appear to be sent from
  # If absent, mail is sent from "please-change-me-at-config-initializers-devise@example.com"
  config.mailer_sender = 'admin@phasetrial.com'

  # If using rails-api, you may want to tell devise to not use ActionDispatch::Flash
  # middleware b/c rails-api does not include it.
  # See: http://stackoverflow.com/q/19600905/806956
  config.navigational_formats = [:json]

  config.secret_key = '828582522bb962a260a8b203d7ab48cd7bd6917d522ede08537be23f1c5d8730aa0bec817d2561af886e9730b3aa94a3cd38d61f1aa12d8fa97e759e8d97bddb'
end