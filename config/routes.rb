Rails.application.routes.draw do
  scope '/api' do
    mount_devise_token_auth_for 'User', at: 'auth', controllers: {
      confirmations: 'overrides/confirmations',
      passwords: 'overrides/passwords'
    }
    resources :configs, except: [:new, :edit]
    resources :visits
    resources :form_types
    resources :roles
    resources :variables
    resources :users do
      get '/find', on: :collection, to: 'users#find'
    end
    resources :forms do
      get '/find', on: :collection, to: 'forms#find'
    end
    resources :subjects do
      get '/find', on: :collection, to: 'subjects#find'
    end
    resources :sites do
      get '/find', on: :collection, to: 'sites#find'
    end
    resources :form_appearances do
      get '/find', on: :collection, to: 'form_appearances#find'
    end
    resources :variable_appearances do
      get '/find', on: :collection, to: 'variable_appearances#find'
    end
  end
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Serve websocket cable requests in-process
  # mount ActionCable.server => '/cable'
end
